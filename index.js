'use strict';

const fs = require('fs');
const path = require('path');

const SVGSpriter = require('svg-sprite');
const Spritesmith = require('spritesmith');

const printTime = date => {
    return `[${String(date.getHours()).padStart(2, '0')}:` +
        `${String(date.getMinutes()).padStart(2, '0')}:` +
        `${String(date.getSeconds()).padStart(2, '0')}]`;
};

const startTime = new Date();
console.log(`${printTime(startTime)} Starting...`);

const printFinish = () => {
    if (!svgFinished || !pngFinished) return;

    const endTime = new Date();
    console.log(`${printTime(endTime)} Finished after ${endTime.getTime() - startTime.getTime()} ms`);
};

const spritesMode = process.argv.includes('--css') ? 'css' : 'symbol';
const renderType = (spritesMode === 'css') ? 'scss' : 'pug';

const encoding = 'utf8';
const templatesDir = 'templates';
const inputDir = 'input';
const outputDir = 'output';

let svgSprites = false;
let pngSprites = false;
let svgFinished = false;
let pngFinished = false;

const svgoOptions = {
    plugins: [
        {removeViewBox: false},
        {cleanupIDs: false},
        {removeUselessDefs: false}
    ]
};
svgoOptions.plugins.push(
    {convertStyleToAttrs: false},
    {
        removeAttrs: {
            attrs: ['svg:style', 'class', 'fill', 'id', 'data-.*']
        }
    }
);

const svgSpritesConfig = {
    dest: outputDir,
    //log: 'verbose',
    mode: {
        [spritesMode]: {
            dest: '.',
            prefix: '$sprite-%s',
            sprite: 'sprite',
            bust: false,
            layout: 'packed',
            render: {
                [renderType]: {
                    template: path.join(templatesDir, `${spritesMode}.mustache`),
                    dest: `sprite-svg-info.${renderType}`
                }
            }
        }
    },
    shape: {
        dimension: {
            precision: 0
        },
        transform: [
            {
                svgo: svgoOptions
            }
        ]
    },
    svg: {
        transform: [
            svg => {
                const svgResult = svg.replace(/ xml:space="preserve"/g, '');
                return svgResult;
            }
        ]
    }
};

const spriter = new SVGSpriter(svgSpritesConfig);

const pngSpritesArray = [];

fs.readdirSync(inputDir).forEach(file => {
    if (path.extname(file) === '.svg') {
        svgSprites = true;
        const spriteFilePath = path.resolve(inputDir, file);
        spriter.add(
            spriteFilePath,
            file,
            fs.readFileSync(spriteFilePath, encoding)
        );
    } else if (path.extname(file) === '.png') {
        pngSprites = true;
        const spriteFilePath = path.join(inputDir, file);
        pngSpritesArray.push(spriteFilePath);
    }
});

if (svgSprites) {
    spriter.compile((error, result) => {
        if (error) return console.log(error);

        if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

        Object.keys(result[spritesMode]).forEach(fileType => {
            fs.writeFile(result[spritesMode][fileType].path, result[spritesMode][fileType].contents, fileError => {
                if (fileError) throw error;
            });
        });

        svgFinished = true;
        printFinish();
    });
} else {
    svgFinished = true;
    printFinish();
}

if (pngSprites) {
    const spritesmithOptions = {
        src: pngSpritesArray,
        padding: 2
    };

    Spritesmith.run(spritesmithOptions, (error, result) => {
        if (error) return console.log(error);

        if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

        fs.writeFileSync(path.join(outputDir, 'sprite.png'), result.image);

        const properties = result.properties;
        const coordinates = result.coordinates;
        let cssContents = Object.keys(coordinates).map(key => {
            const sprite = coordinates[key];
            const spriteName = path.basename(key, '.png');
            return `$sprite-${spriteName}: ` +
                `(${sprite.x}px, ${sprite.y}px, ` +
                `${sprite.width}px, ${sprite.height}px);`;
        }).join('\n');
        cssContents += `\n$spritesheet: (${properties.width}px, ${properties.height}px);\n`;

        fs.writeFileSync(path.join(outputDir, 'sprite-svg-info.scss'), cssContents, encoding);

        pngFinished = true;
        printFinish();
    });
} else {
    pngFinished = true;
    printFinish();
}